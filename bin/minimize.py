#!/usr/bin/env python3

# Copyright Louis Paternault 2017
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sys
import math
import textwrap

from PIL import Image

def multigcd(*numbers):
    """Return the gdc of the arguments."""
    result = numbers[0]
    for n in numbers:
        result = math.gcd(n, result)
    return result

def usage():
    print("minimize.py SOURCE DEST [SIZE]")
    print(textwrap.dedent("""
    Read source image, and reduce it, each pixel of the destination image containing the average of the pixels of a SIZE×SIZE square of the source image.

    If SIZE is not given:
    * if the source image is pixellated (that is, made of squares of size N of identical color), use N;
    * else, use 1 (in this case, the destination image is a copy of the source image).
    """))

def guess_pixelsize(filename):
    """Guess the size of pixels of filename.

    If image `filename` is pixellated (that is, made of squares of size N
    of identical colors), return N. Else, return 1.
    """
    candidates = set()
    with Image.open(filename).convert("RGB") as original:
        for image in original, original.transpose(Image.TRANSPOSE):
            for y in range(image.size[1]):
                candidate = 0
                last = None
                for x in range(image.size[0]):
                    if last is None or last == image.getpixel((x, y)):
                        candidate += 1
                    else:
                        candidates.add(candidate)
                        candidate = 1
                    last = image.getpixel((x, y))
                candidates.add(candidate)
        return multigcd(*candidates)

def mean(image, pixelsize, startx, starty):
    """Return the mean color of a square of pixels.

    Pixels are read from `image`, from coordinates `(startx, starty)`, in
    a `pixelsize`×`pixelsize` square.
    """
    rgb = (0, 0, 0)
    for x in range(startx*pixelsize, startx*pixelsize+pixelsize):
        for y in range(starty*pixelsize, starty*pixelsize+pixelsize):
            rgb = (
                rgb[0] + image[x, y][0],
                rgb[1] + image[x, y][1],
                rgb[2] + image[x, y][2],
                )
    return (
        rgb[0]//(pixelsize**2),
        rgb[1]//(pixelsize**2),
        rgb[2]//(pixelsize**2),
        )

def minimize(sourcename, destname, pixelsize):
    """Reduce source image."""
    with Image.open(sourcename).convert("RGB") as source:
        size = source.size
        with Image.new('RGB', (size[0]//pixelsize, size[1]//pixelsize)) as dest:
            srcpix = source.load()
            dstpix = dest.load()
            for x in range(size[0]//pixelsize):
                for y in range(size[1]//pixelsize):
                    dstpix[x, y] = mean(srcpix, pixelsize, x, y)
            dest.save(destname)

if __name__ == "__main__":
    if len(sys.argv) == 3:
        minimize(sys.argv[1], sys.argv[2], guess_pixelsize(sys.argv[1]))
        sys.exit(0)
    elif len(sys.argv) == 4:
        minimize(sys.argv[1], sys.argv[2], int(sys.argv[3]))
        sys.exit(0)
    usage()
    sys.exit(1)
