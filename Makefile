LUALATEX=lualatex
LATEX=latex

.PHONY: all
all: build logo doc

.PHONY: doc
doc: pixelart0.pdf pixelart.pdf

################################################################################
# Build

.PHONY: build
build: \
	pixelart.lua pixelart.tex pixelart.sty pixelart.pdf \
	pixelart0.tex pixelart0.sty pixelart0.pdf \
	README.md LICENSE.txt CHANGELOG.md
	ctanify --notds --pkgname=pixelart $^

################################################################################
# Documentation

pixelart0.pdf: pixelart0.tex pixelart0.sty
	$(LUALATEX) pixelart0.tex
	makeindex -s gglo.ist -o pixelart0.gls pixelart0.glo
	makeindex -s gind.ist -o pixelart0.ind pixelart0.idx
	$(LUALATEX) pixelart0.tex
	$(LUALATEX) pixelart0.tex

pixelart.pdf: pixelart.tex pixelart.sty
	$(LUALATEX) pixelart.tex
	$(LUALATEX) pixelart.tex

################################################################################
# Logo

.PHONY: logo
logo: logo.png pixelart.png

logo.pdf: logo.tex pixelart0.sty
	$(LUALATEX) logo

logo.png: logo.pdf
	convert -density 1000 logo.pdf[0] -resize 200x200 logo.png

pixelart.png: logo.pdf
	convert -density 1000 logo.pdf[1] -resize 400x400 pixelart.png
